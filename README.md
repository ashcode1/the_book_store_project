# README #

* To get this application up and running - download/clone the source files to a local folder and open the index.html in the browser. 

### What is this repository for? ###

* This repo contains a project that combines HTML, CSS and Javascript to present a basic product page displaying books.

* It implements Google's Books API to fetch data.

### Summary ###

* This is based on a mobile-first design, achieving both mobile and desktop layouts in a single style-sheet.

* The CSS is written WITHOUT any framework and loosely based on the CSS grid system.

* Each book in the list displays the book cover, title, subtitle, all authors, number of pages and desription (fetched using the API).

* The product cards (book cover/book info/any space within the border) can be hovered over and clicked-on and un-clicked. I used colour changes to indicate this. 

* The "Featured" column displays the last two books from the JSON array.

* The book descriptions are limited to 140 characters.

* The mobile layout features a "Burger Menu" drop down, made WITHOUT the use of Javascript.

* Although I have included the toggled class "is-selected", unfortunately I didn't manage to implement maintining state on refresh.

* If I attempted this project again I would try to use flex-box from the very begginning instead of relying on CCS grid - which ended up being akward on layout adjustment.

* I had some issues trying to flip the divs that contain the books, so was un-able to send "Featured" books to the right-hand column on desktop mode. Again, this would be made much easier with flex.

* Thanks for taking the time to have a look through this. I would be very grateful for any feedback.

### Who do I talk to? ###

* Repo owner - Ashley Hopkins