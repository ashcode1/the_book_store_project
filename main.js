// HELPER FUNCTIONS

// Create Node
function createNode(element) {
  return document.createElement(element);
}

// Append
function append(parent, el) {
  return parent.appendChild(el);
}

// Book description limited to 140 characters (137 + ... to indicate overflow)
function characters140(text) {
  let split = text.split(""),
      slice = split.slice(0, 137);
  return description140ch = slice.join("") + "..."; 
}

// If no subtitle exists, return empty string instead of "undefined"
function defined (subtitle) {
  if ("undefined") {
    return "";
  } else {
    return item.volumeInfo.subtitle;
  }
}

// Spacing between authors added after dividing comma 
function addSpaceBetween (authors) {
  return authors.map(function(author, i) {
    author = author.trim()
      return " " + author;
  });        
}

// API URL
const api = 'https://www.googleapis.com/books/v1/volumes?q=HTML5';

// GET FEATURED BOOKS
fetch(api)
.then((resp) => resp.json())
.then(function(data) {
  let items = data.items;

  // Select last two books from the end of the array 
  let slicedArr = items.slice(items.length -2, items.length);
  // Switch them around so last in the array is first featured
  let featuredList = slicedArr.reverse();
  
  // Map selected book info to "featured" list 
  return featuredList.map(function(item) {
    const ol = document.getElementById('featured');
    console.log(ol);
    let li = createNode('li'),
      a = createNode('a'),
      img = createNode('img'), 
      span = createNode('span'),
      // create divs for separation of img and span
      divImg = createNode('div'),
      divSpan = createNode('div');
      // assign values to elements to be mapped
      li.id = "book_" + items.indexOf(item).toString();
      li.className = "book";
      li.onclick = function () {
        li.classList.toggle('is-selected');
      }
      a.setAttribute('href', "#"); 
      console.log(a);                     
      img.src = item.volumeInfo.imageLinks.smallThumbnail;
      span.innerHTML = `<h3>${item.volumeInfo.title}</h3> 
                        <h3>${defined(item.volumeInfo.subtitle)}</h3> 
                        <h5>${addSpaceBetween(item.volumeInfo.authors)}</h5> 
                        <h6>Pages: ${item.volumeInfo.pageCount}</h6>
                        <h6>${characters140(item.volumeInfo.description)}</h6>`;
                        // Helper functions used here
    
    append(li, a);
    // place image in div, div in list & span in list                    
    append(divImg, img);
    append(divSpan, span);
    append(li, divImg);
    append(li, divSpan);
    append(ol, li);
    })
  })
  
// GET ALL BOOKS 
fetch(api)
.then((resp) => resp.json())
.then(function(data) {

  // Map slected info of all books to list of books
  let items = data.items;
  return items.map(function(item, i) {
    const ol = document.getElementById('items');
    let li = createNode('li'),
      a = createNode('a'),
      img = createNode('img'), 
      span = createNode('span'),
      // create divs for separation of img and span
      divImg = createNode('div'),
      divSpan = createNode('div');
      // assign values to elements to be mapped 
      li.id = "book_" + items.indexOf(item).toString();
      li.className = "book";
      li.onclick = function () {
        li.classList.toggle('is-selected');
      }
      a.setAttribute('href', "#"); 
      console.log(a);                     
      img.src = item.volumeInfo.imageLinks.smallThumbnail;
      span.innerHTML = `<h3>${item.volumeInfo.title}</h3> 
                        <h3>${defined(item.volumeInfo.subtitle)}</h3> 
                        <h5>${addSpaceBetween(item.volumeInfo.authors)}</h5> 
                        <h6>Pages: ${item.volumeInfo.pageCount}</h6>
                        <h6>${characters140(item.volumeInfo.description)}</h6>`;
                        // Helper functions used here
    
    append(li, a);
    // place image in div, div in list & span in list                    
    append(divImg, img);
    append(divSpan, span);
    append(li, divImg);
    append(li, divSpan);
    append(ol, li);
    })
  })

// CATCH ERRORS
.catch(function(error) {
  console.log(JSON.stringify(error));
});  

